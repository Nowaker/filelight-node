/**
 * Module dependencies.
 */

var express = require('express'),
  http = require('http'),
  path = require('path'),
  statDir = require('./filelight').statDir;

var app = express();

function parseSize(size) {
  if(size > 1024*1024*1024) {
    size /= 1024*1024*1024;
    size = parseFloat(size).toFixed(2);
    size += ' GB';
  }
  else if(size > 1024*1024) {
    size /= 1024*1024;
    size = parseFloat(size).toFixed(2);
    size += ' MB';
  }
  else if(size > 1024) {
    size /= 1024;
    size = parseFloat(size).toFixed(2);
    size += ' KB';
  }
  else {
    size += ' B';
  }
  return size;
};

app.configure(function() {
  app.set('port', process.env.PORT || 3003);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

var cache = {};

app.get(/\/dir\/(.*)/, function(req, res) {
  var dir = req.params[0];
  if(dir.length === 0) dir = '';

  var result = statDir(dir);

  var sum = result.totalSize;

  var entries = result.files.map(function(f) {
    var size = f.size;
    if(f.isDirectory())
      size = f.dirData.totalSize;
    f.percent = 100 * size / sum;
    f.name = f.filename;

    f.params = {};
    f.params.isDirectory = f.isDirectory();
    f.params.parsedSize = parseSize(size);
    f.params.fullpath = f.fullpath;

    return f;
  });

  res.render('index', {
    entries: entries
  });
});

http.createServer(app).listen(app.get('port'), function() {
  console.log("Express server listening on port " + app.get('port'));
});